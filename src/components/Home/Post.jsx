import React, { Component } from 'react'

export default class Post extends Component {
    render() {
        let id = this.props.match.params.id;
        return (
            <div className="container">
                <h1>This is content from post {id}</h1>
            </div>
        )
    }
}

