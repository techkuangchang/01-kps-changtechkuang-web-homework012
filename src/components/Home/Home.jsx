import React from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

function Home() {
    return (
        <div className="container"> 
            <br/>
            <div className="Card">
            <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg"/>
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="light">
                <Link to={`/Post/1`}>
                Go somewhere
                </Link>
                </Button>
            </Card.Body>
            </Card>

            <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg"/>
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="light">
                <Link to={`/Post/1`}>
                Go somewhere
                </Link>
                </Button>
            </Card.Body>
            </Card>

            <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg"/>
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="light">
                <Link to={`/Post/1`}>
                Go somewhere
                </Link>
                </Button>
            </Card.Body>
            </Card>

            <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg"/>
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="light">
                <Link to={`/Post/1`}>
                Go somewhere
                </Link>
                </Button>
            </Card.Body>
            </Card>

            <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg"/>
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="light">
                <Link to={`/Post/1`}>
                Go somewhere
                </Link>
                </Button>
            </Card.Body>
            </Card>

            <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg"/>
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="light">
                <Link to={`/Post/1`}>
                Go somewhere
                </Link>
                </Button>
            </Card.Body>
            </Card>

            <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg"/>
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="light">
                <Link to={`/Post/1`}>
                Go somewhere
                </Link>
                </Button>
            </Card.Body>
            </Card>

            <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src="https://smalltownwashington.com/wp-content/uploads/2020/02/heart-3147976_1920.jpg"/>
            <Card.Body>
                <Card.Title>Card Title</Card.Title>
                <Card.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card.Text>
                <Button variant="light">
                <Link to={`/Post/1`}>
                Go somewhere
                </Link>
                </Button>
            </Card.Body>
            </Card>
            </div>
        </div>
    )
}

export default Home
