import React from 'react'
import {Link, useRouteMatch, Switch, Route, BrowserRouter as Router} from 'react-router-dom'
import Animation from './Animation'
import Movie from './Movie';

function Video() {
    let {path} = useRouteMatch();
    return(
        <Router>
            <div className="container">               
                <ul>
                    <li>
                        <Link to='/.video/animation'>Animation</Link>
                    </li>
                    <li>
                        <Link to="/.video/movie">Movie</Link>
                    </li>
                </ul>
            </div>
            <hr/>
            <Switch>
                <Route path={path}>
                    <h3 className="container">Please select a topic1.</h3>
                </Route>
                <Route path="/.video/animation">
                    <Animation/>
                </Route>
                <Route path="/.video/movie">
                    <Movie/>
                </Route>
            </Switch>
        </Router>
    );
}

export default Video
