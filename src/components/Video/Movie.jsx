import React from 'react'
import {useParams, Link, useRouteMatch, Switch, Route} from 'react-router-dom'

function Movie() {
    let {path , url} = useRouteMatch();
    return (
        <div className="container">
            <h3>Movie Category</h3>
            <ul>
                <li>
                    <Link to={`${url}/advanture`}>Advanture</Link>
                </li>
                <li>
                    <Link to={`${url}/comedy`}>Comedy</Link>
                </li>
                <li>
                    <Link to={`${url}/crime`}>Crime</Link>
                </li>
                <li>
                    <Link to={`${url}/documentary`}>Documentary</Link>
                </li>
            </ul>

            <Switch>
                <Route exact path={path}>
                    <h3>Please select a topic.</h3>
                </Route> 
                <Route path={`${path}/:movieId`}>
                    <MovieCat/>
                </Route>
            </Switch>
        </div>
    );
}

function MovieCat(){
    let {movieId} = useParams();
    return(
        <div>
            <h3>{movieId}</h3>
        </div>
    )
}

export default Movie
