import React from 'react'
import {useParams, Link, useRouteMatch, Switch, Route} from 'react-router-dom'

function Animation() {
    let {path , url} = useRouteMatch();
    return (
        <div className="container">
            <h3>Animation Category</h3>
            <ul>
                <li>
                    <Link to={`${url}/action`}>Action</Link>
                </li>
                <li>
                    <Link to={`${url}/romance`}>Romance</Link>
                </li>
                <li>
                    <Link to={`${url}/comedy`}>Comedy</Link>
                </li>
            </ul>

            <Switch>
                <Route exact path={path}>
                    <h3>Please select a topic.</h3>
                </Route> 
                <Route path={`${path}/:animationId`}>
                    <AnimaionCat/>
                </Route>
            </Switch>
        </div>
    );
}

function AnimaionCat(){
    let {animationId} = useParams();
    return(
        <div>
            <h3>{animationId}</h3>
        </div>
    )
}

export default Animation
