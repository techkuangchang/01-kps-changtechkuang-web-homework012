import React from 'react'
import {BrowserRouter as Router,Link,useLocation} from "react-router-dom"

function Account() {
    return (
        <Router>
            <QueryParams/>
        </Router>
    );
}

function useQuery(){
    return new URLSearchParams(useLocation().search);
}
    
function QueryParams(){
    let query = useQuery();
    return(
        <div className="container">
            <div>
                <h2>Account</h2>
                <ul>
                    <li>
                        <Link to="/account?name=netflix">Netflix</Link>
                    </li>
                    <li>
                        <Link to="/account?name=zillow-group">Zillow Group</Link>
                    </li>
                    <li>
                        <Link to="/account?name=yahoo">Yahoo</Link>
                    </li>
                    <li>
                        <Link to="/account?name=modus-create">Modus Createx</Link>
                    </li>
                </ul>

                <Child name={query.get('name')}/>
            </div>
        </div>
    );
}

function Child(props){
    return(
        <div>
            {props.name ? (
                <h3>
                    The <code>name</code> in the query string is &quot;{props.name}
                    &quot;
                </h3>
            ) : (
                <h3>There is no name in the query string</h3>
            )}
        </div>
    );
}

export default Account
