import React from "react";
import {Form } from "react-bootstrap";
import Check from './Check'

function Auth(props) {
  return (
    <div className="container"><br/>
        <Form>
        
          <input type="text" placeholder="Username"/>
          <input type="password" placeholder="Password"/>
       
        <button
          onClick={() => {
            Check.login(() => {
              props.history.push("/Welcome");
            });
          }}
        >
          Login
        </button>
      </Form>
    </div>
  );
}

export default Auth;
