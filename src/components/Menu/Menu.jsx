import React from 'react'
import {Navbar, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'

function Menu() {
    return (
        <div>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <div className="container">
            <Navbar.Brand as = {Link} to="/">Router</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                <Nav.Link as = {Link} to="/home">Home</Nav.Link>
                <Nav.Link as = {Link} to="/video">Video</Nav.Link>
                <Nav.Link as = {Link} to="/account">Account</Nav.Link>
                <Nav.Link as = {Link} to="/auth">Auth</Nav.Link>
                </Nav>
            </Navbar.Collapse>
            </div>
            </Navbar>
        </div>
    )
}

export default Menu
