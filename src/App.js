import React from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import {BrowserRouter as Rotuer, Switch, Route} from 'react-router-dom'
import Video from './components/Video/Video'
import Account from './components/Account/Account'
import Menu from './components/Menu/Menu'
import Home from './components/Home/Home'
import Main from './components/Main'
import Post from './components/Home/Post' 
import Auth from './components/Auth/Auth'
import {ProtectedRoute} from './components/Auth/ProtectedRoute'
import Welcome from './components/Auth/Welcome'
import NotFound from './components/NotFound';

function App() {
  return (
    <div className="App">
      <Rotuer>
      <Menu/>
        <Switch>
        <Route path='/' exact component={Main}/>
        <Route path='/Home' component={Home}/>
        <Route path='/Post/:id' component={Post}/>
        <Route path='/Video' component={Video}/>
        <Route path='/Account' component={Account}/>
        <Route path='/Auth' component={Auth}/>
        <ProtectedRoute path='/Welcome' component={Welcome}/>
        <Route path='*' component={NotFound}/>
        </Switch>
      </Rotuer>
    </div>
  );
}

export default App;
